# To check the existence of files
# 
# Standard imports
import csv
import pandas as pd

def lfmDBCheck():
    try:
        open("res/lfm.csv")
        lfmDB = 'res/lfm.csv'
        return lfmDB
    except IOError:
        with open("res/lfm.csv", "w+") as outfile:
            writer = csv.writer(outfile)
            writer.writerow(["ID", "Username"])
            writer.writerow([999999999999999999, "Placeholder"])
            outfile.close()
            print("\n" + "Last.FM database made at ./res/lfm.csv")
            lfmDB = 'res/lfm.csv'
            return lfmDB