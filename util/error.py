from discord.ext import commands
import discord
import sys
import traceback
import math

class error(commands.Cog):
    """ Error handling (has no commands) """
    def __init__(self, bot):
        self.bot = bot

    @commands.Cog.listener()
    async def on_command_error(self, ctx, error):
        # if command has local error handler, return
        if hasattr(ctx.command, 'on_error'):
            return

        # get the original exception
        error = getattr(error, 'original', error)

        if isinstance(error, commands.CommandNotFound):
            await ctx.send("\U0001f6ab | Command not found")
            return

        if isinstance(error, commands.BotMissingPermissions):
            missing = [perm.replace('_', ' ').replace('guild', 'server').title() for perm in error.missing_perms]
            if len(missing) > 2:
                fmt = '{}, and {}'.format("**, **".join(missing[:-1]), missing[-1])
            else:
                fmt = ' and '.join(missing)
            _message = f'\U0001f6ab | I need the **{fmt}** permission(s) to run this command.'
            await ctx.send(_message)
            return

        if isinstance(error, commands.MissingPermissions):
            missing = [perm.replace('_', ' ').replace('guild', 'server').title() for perm in error.missing_perms]
            if len(missing) > 2:
                fmt = '{}, and {}'.format("**, **".join(missing[:-1]), missing[-1])
            else:
                fmt = ' and '.join(missing)
            await ctx.send(f'\U0001f6ab | You need the **{fmt}** permission(s) to use this command.')
            return

        if isinstance(error, discord.Forbidden):
            await ctx.send("\U0001f6ab | I can't do that.")


        if isinstance(error, commands.NoPrivateMessage):
            try:
                await ctx.author.send('\U0001f6ab | This command cannot be used in direct messages.')
            except discord.Forbidden:
                pass
            return

        if isinstance(error, commands.CheckFailure):
            await ctx.send("\U0001f6ab | You do not have permission to use this command.")
            return

        if isinstance(error, commands.MissingRequiredArgument):
            await ctx.send(f"\U0001f6ab | Missing Parameter: **{error.param}**")
   
        if isinstance(error, commands.BadArgument):
            await ctx.send(f"\U0001f6ab | Bad argument. Check your command and try again.")

        if isinstance(error, commands.CommandOnCooldown):
            await ctx.send(f"\U0001f6ab | You are on cooldown. Please wait **{math.ceil(error.retry_after)} seconds** before trying again.")

        # ignore all other exception types, but print them to stderr
        print('\U0001f6ab | Ignoring exception in command {}:'.format(ctx.command), file=sys.stderr)

        traceback.print_exception(type(error), error, error.__traceback__, file=sys.stderr)

def setup(bot):
    bot.add_cog(error(bot))
