# To check for things
#
# Standard imports
import os 
import re
import datetime
import json

# Check for spotify secret
def spotifyCheck(spot):
    if spot['Secret'] != "Spotify Secret":
        clientSecret = spot['Secret']
        clientID = spot['ID']
        os.environ['SPOTIPY_REDIRECT_URI'] = "localhost"
        return clientID, clientSecret
        return True
    else: 
        print("\nCommands that use spotify have been disabled because the check for your spotify secret has failed.")
        return False


# Check for Google CSE API Key
def googleCheck(googleImg):    
    if googleImg['API Key'] != 'Google API Key':
        googleKey = googleImg['API Key']
        googleCx = googleImg['Google CX']
        return googleKey, googleCx
        return True
    else:
        print("\nGoogle Images commands have been disabled.")
        return False

# Check for logging channel
def logCheck(channels):
    if channels['Log'] != 'Log Channel ID':
        chanRules = channels['Rules']
        chanLog = channels['Log']
        return chanRules, chanLog
        return True
    else:
        print("\nAdmin commands and Logging have been disabled")
        return False

# Check to see if roles have been configured
def roleCheck(roles):
    if roles[0] != "Role 1":
        return True  
    else:
        print("\nRole commands have been disabled")
        return False

# Check for last.fm API key
def lfmCheck(lfm):
    if lfm['API key'] != "Last.FM API Key":
        api = lfm['API key']
        secret = lfm['Secret']
        return api, secret
        return True
    else:
        print("\n" + "Music commands have been disabled")
        return False
