import discord
from discord.ext import commands
import subprocess
import sys
import asyncio


class owner(commands.Cog):
    """ You must be owner to use these. """
    def __init__(self, bot):
        self.bot = bot

    @commands.command()
    @commands.is_owner()
    async def restart(self, ctx):
        """Restarts the bot"""
        await ctx.send("Restarting...")
        await asyncio.sleep(1)
        subprocess.call([sys.executable, "alice.py"])
        await ctx.send("Done!")
        await self.bot.logout()
    
    @commands.command()
    @commands.is_owner()
    async def stop(self, ctx):
        """ Kills the bot. Must be manually restarted. """
        await ctx.send("Bye!")
        await self.bot.logout()


def setup(bot):
    bot.add_cog(owner(bot))