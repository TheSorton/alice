import requests
from discord.ext import commands
import discord


class image(commands.Cog):
    """ Image commands """
    def __init__(self, bot):
        self.bot = bot

    @commands.command()
    async def pape(self, ctx):
        """Get a pape.\n\n**Syntax**\n`pape query (optional)`\nIf no query is given, it returns a random wallpaper"""
        message = ctx.message
        footer_url = 'https://pbs.twimg.com/profile_images/653341480640217088/t1c1aTc9.png'
        url = 'https://wallhaven.cc/api/v1/search'
        params = {'sorting': 'random', 'seed': 'Lain'}

        # In case a query exists (a!pape query).
        if len(message.content.split()) >= 2:
            query = ' '.join(message.content.split()[1:])
            params['q'] = query

        response = requests.get(url, params)

        if len(response.json()['data']) > 0:
            wallpaper_url = response.json()['data'][0]['url']
            wp = response.json()['data'][0]['path']
            embed = (discord.Embed(title='Wallpaper', description=f"[Source]({wallpaper_url})",  colour=0xbe132d)
                            .set_footer(text='This command is powered by wallhaven.cc',icon_url=footer_url)
                            .set_image(url=wp))

            await ctx.send(embed=embed)

        else:
            await ctx.send('No results found')

def setup(bot):
    bot.add_cog(image(bot))