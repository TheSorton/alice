import discord
from discord.ext import commands
import json
from alice import roles, prefix
from util import checks


class role(commands.Cog):
    """ Role commands """
    def __init__(self, bot):
        self.bot = bot

    @commands.command()
    async def iam(self, ctx, role):
        """ Add a role\n\n**Syntax**\n`iam role`"""
        member = ctx.message.author
        role = role.lower()
        found = 0
        for r in roles:
            if role == r:
                found = 1
        if found == 0:
            await ctx.send(f"That role is not self-assignable, or you made a mistake. Use `{prefix}lsar` to see available roles.")
            return
        roleObj = discord.utils.get(ctx.message.author.guild.roles, name=f'{role}')
        await member.add_roles(roleObj)
        await ctx.send(f'**{role}** has been added!')

    @commands.command(aliases=['iamnot'])
    async def iamn(self, ctx, role):
        """ Remove a Role\n\n**Syntax**\n`iam (or iamnot) role`"""
        member = ctx.message.author
        role = role.lower()
        found = 0
        for r in roles:
            if role == r:
                found = 1
                for i in member.roles:
                    if role == i.name:
                        if role != 'silenced': 
                            await member.remove_roles(i)
                            await ctx.send(f'**{i.name}** has been removed!')
                            break
                        else: 
                            await ctx.send('Silenced cant be removed.')
                            break
                else:
                    await ctx.send("You don't have that role.")
                    
        if found == 0:
            await ctx.send(f"That role is not self-assignable, or you made a mistake. Use `{prefix}lsar` to see available roles.")
            return

    @commands.command()
    async def lsar(self, ctx):
        """ List assignable roles\n\n**Syntax**\n`lsar` """
        embed = (discord.Embed(color=0xdc143c)
                 .set_author(name="Self-assignable roles")
                 .add_field(name="Roles", value="\n".join(roles)))
        await ctx.send(embed=embed)

def setup(bot):
    if bool(checks.roleCheck(roles)):
            bot.add_cog(role(bot))
