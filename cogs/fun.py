import random
from discord.ext import commands
import discord

class fun(commands.Cog):
    """ Fun (or misc) commands """
    def __init__(self, bot):
        self.bot = bot
        self.ask_replies = ["It is certain", "It is decidedly so", "Without a doubt", "Yes definitely",
                              "You may rely on it", "As I see it yes", "Most likely", "Outlook good", "Yes",
                              "Signs point to yes", "Reply hazy. Try again", "Ask again later",
                              "Better not tell you now", "Cannot predict now", "Concentrate and ask again",
                              "No comment", "Don't count on it", "My reply is no", "My sources say no",
                              "Outlook not so good", "Very doubtful", "Not as I see it", "No. Never", "Absolutely not",
                              "I doubt it", "What the fuck...?"]

    @commands.command()
    async def flip(self, ctx):
        """Flip a coin\n\n**Syntax**\n`flip`"""
        if random.getrandbits(1):
            await ctx.send(content="Heads!")
        else:
            await ctx.send(content="Tails!")

    @commands.command(aliases=['ball', '8ball'])
    async def ask(self, ctx, *, question):
        """ask alice something\n\n**Syntax**\n`ball question`\nQuestion is required."""
        await ctx.send(f'\U0001f3b1 | {random.choice(self.ask_replies)}')

    @commands.command()
    async def choose(self, ctx, *, choices):
        """Have alice choose from a list of choices.\n\n**Syntax**\n`choose x, y, etc.`\nYou must supply at least two choices."""
        choice_list = choices.split(', ')
        if len(choice_list) < 2:
            return await ctx.send("I need at least two choices seperated with a comma and a space.")

        await ctx.send(f' I pick **{random.choice(choice_list)}**') 
    
    @commands.command()
    async def dice(self, ctx, amount: int = 1):
        """ Roll X 6-sided dice\n\n**Syntax**\n`dice number (default 1)` """
        if amount < 1:
            return await ctx.send('You actually have to roll a die...')
        if amount > 20:
            return await ctx.send("I don't have that many dice!")

        emb = discord.Embed(title=':game_die: Dice roll')
        scores = [random.randint(1, 6) for _ in range(amount)]
        if len(scores) == 1:
            emb.add_field(name='Score', value=f'You rolled: {"".join(str(scores[0]))}.')
            return await ctx.send(embed=emb)

        emb.add_field(name='Rolled', value=f"{', '.join((str(x) for x in scores))}", inline=False)
        emb.add_field(name='Total Score', value=f'{sum(scores)}', inline=False)
        await ctx.send(embed=emb)

def setup(bot):
            bot.add_cog(fun(bot))
