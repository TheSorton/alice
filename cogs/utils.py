import os
import requests
import discord
import platform
import time, datetime
from discord.ext import commands
from colorthief import ColorThief
from alice import prefix

start_time = time.time()

class utils(commands.Cog):
    """ Utility commands """
    def __init__(self, bot):
        self.bot = bot
        self.prefix = prefix

    # Utils base methods.
    def find_member(self, message, nickname):
        """
        Finds the first memeber that matches the nickname
        on the guild where the message was sent.

        Parameters
        ----------
        message : discord.Message
            Message that triggered the event.
        nickname : str
            nickname of the user that might be
            on the same guild where the message
            was delivared.

        Returns
        -------
        member : discord.Member
            First discord member that matches the nickname.
            If no member was found that matches the nickname
            None will be returned.
        """
        for member in message.guild.members:
            if nickname.lower() in member.display_name.lower():
                return member

    def create_avatar_embed(self, message, user):
        """
        Creates an embed object that will contain the avatar
        of the user and will 'mention' the author of the original
        message.

        Paramters
        ---------
        message : discord.Message
            Message that triggered the event.
        user : discord.Member
            User from which it's avatar is going to be retrieved.

        Returns
        -------
        embed : discord.Embed
            embed containing the avatar of the user.
        """
        requestor = message.author
        name = user.name
        avatarImage = user.avatar_url_as(static_format='png', size=1024)
        response = requests.get(avatarImage)
        with open('img.png', 'wb') as h:
            h.write(response.content)

        color_thief = ColorThief('img.png')
        dominant_color = color_thief.get_color(quality=1)
        os.remove('img.png')
        clr = '0x' + '%02X%02X%02X' % dominant_color
        clr = int(clr, base=16)
        embed = (discord.Embed(title=f"Avatar of {name}", value=requestor, color=clr)
                        .set_image(url=avatarImage))

        return embed

    # Cog commands.
    @commands.command()
    async def say(self, ctx, *, arg):
        """Repeat what you say (unless you're thnyan)\n\n**Syntax**\n`say something`"""
        if ctx.author.id == 98773013649977344:
            await ctx.send(content='No.')
            return

        await ctx.send(arg)

    @commands.command()
    async def code(self, ctx):
        await ctx.send("https://gitlab.com/TheSorton/alice")
        
    @commands.command()
    async def avatar(self, ctx):
        """Get an avatar \n\n**Syntax**\n`avatar User or @User (both optional)`\nWill get your avatar if no user is specified."""
        message = ctx.message
        if len(message.mentions) > 0:
            for user in message.mentions:
                print(user.avatar_url)
                embed = self.create_avatar_embed(message, user)
                await ctx.send(embed=embed)

        # If the message contains users but doesn't metion them.
        elif len(message.content.split()) == 2:
            # Getting the users and removing nicknames that didn't match (None).
            nicknames = message.content.split()[1:]
            users = [self.find_member(message, nickname) for nickname in nicknames]
            users = [user for user in users if user is not None]

            for user in users:
                print(user.avatar_url)
                embed = self.create_avatar_embed(message, user)
                await ctx.send(embed=embed)

        else:
                embed = self.create_avatar_embed(message, message.author)
                await ctx.send(embed=embed)

    @commands.command()
    async def memcount(self, ctx):
        """Counts all the members in a guild\n\n**Syntax**\n`memcount`"""
        message = ctx.message
        number = message.guild.member_count
        await ctx.send(f'This server has {number} members.')

    @commands.command()
    async def userinfo(self, ctx, *, target = None):
        """Get user info\n\n**Syntax**\n`userinfo id or User, or @User (optional)`\nWill pull up your info if no user is given."""
        message = ctx.message

        if target is None:
            target = message.author

        # When the userinfo request is given an ID
        elif len(target) == 18:
            target = await message.guild.fetch_member(int(target))

        # When the userinfo is given a nickname.
        elif len(message.mentions) == 0:
            target = self.find_member(message, message.content.split()[1])

        # When the userinfo is given a mention.
        else:
            target = message.mentions[0]
        
        # Format dates
        created = target.created_at.strftime("%m/%d/%Y, %H:%M:%S")
        joined = message.guild.get_member(target.id).joined_at.strftime("%m/%d/%Y, %H:%M:%S")

        # Create embed
        roles = [role for role in target.roles]
        embed = (discord.Embed(color=target.color)
                        .set_author(name=f"{target.name}'s information", icon_url=target.avatar_url_as(static_format='png'))
                        .add_field(name="ID", value=f'{target.id}', inline=False)
                        .add_field(name="Account Created",
                                value=f'{created} UTC',
                                inline=True)
                        .add_field(name="Joined",
                                value=f'{joined} UTC',
                                inline=False)
                        .add_field(name="Status",
                                value=f'{target.status}',
                                inline=False)
                        .add_field(name=f'Roles ({len(roles)})',
                                value=" ".join([role.mention for role in roles]))
                        .set_thumbnail(url=target.avatar_url_as(static_format='png')))

        await ctx.send(embed=embed)

    @commands.command()
    async def stats(self, ctx):
        """Get bot stats\n\n**Syntax**\n`stats`"""
        current_time = time.time()
        difference = int(round(current_time - start_time))
        text = str(datetime.timedelta(seconds=difference))

        embed = (discord.Embed(title='Stats', colour=ctx.message.author.top_role.colour)
                        .set_author(name=self.bot.user.name, icon_url=self.bot.user.avatar_url_as(static_format='png'))
                        .add_field(name="Discord.py Version", value=discord.__version__, inline=False)
                        .add_field(name="Python Version", value=platform.python_version(), inline=False)
                        .add_field(name="Uptime", value=text, inline=False))

        await ctx.send(embed=embed)

    @commands.command()
    async def emoji(self, ctx, emote : discord.PartialEmoji):
         await ctx.send(emote.url)

def setup(bot):
            bot.add_cog(utils(bot))
