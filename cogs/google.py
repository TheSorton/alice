import discord
from discord.ext import commands
import os
import requests
from util import checks
from alice import googleImg
import urllib


class google(commands.Cog):
    """ Google commands """
    def __init__(self, bot, ctx):
        self.bot = bot
        if bool(checks.googleCheck(googleImg)):
            (self.key, self.cx) = checks.googleCheck(googleImg)

    @commands.command()
    async def image(self, ctx, *, query):
        """Image command. \n\n **Syntax** \n `image query` """
        url = f'https://www.googleapis.com/customsearch/v1?key={self.key}&cx={self.cx}&safe=high&searchType=image&q={urllib.parse.quote(query)}'
        
        response = requests.get(url)
        if response.json()['searchInformation']['totalResults'] == "0":
            await ctx.send('No results found')
        else: 
            imageURL = response.json()['items'][0]['link']
            title = response.json()['items'][0]['title']
            link = response.json()['items'][0]['image']['contextLink']
            embed = (discord.Embed(title=f'You searched for {query}', description=f'[{title}]({link})')
                            .set_image(url=imageURL))
            await ctx.send(embed=embed)

def setup(bot):
    if bool(checks.googleCheck(googleImg)):
        ctx = commands.Context
        bot.add_cog(google(bot, ctx))