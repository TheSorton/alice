import discord
from discord.ext import commands

class admin(commands.Cog):
    """ Admin commands """
    def __init__(self, bot):
        self.bot = bot

    @commands.command()
    @commands.has_permissions(kick_members=True)
    async def kick(self, ctx):
        """Kicks a member\n\n**Syntax**\n`kick @User`"""
        message = ctx.message

        if len(message.mentions) > 0:
            for user in message.mentions:
                server = user.guild

                await server.kick(user)
                await ctx.send(f'**{user}** has been kicked')

    @commands.command()
    @commands.has_permissions(ban_members=True)
    async def ban(self, ctx):
        """Bans a member\n\n**Syntax**\n`ban @User Reason`\nIf a reason is not given, alice will put "No reason given" as the ban reason in Discord's audit logs."""
        message = ctx.message

        if len(message.mentions) > 0:
            for user in message.mentions:
                server = user.guild
                args = message.content.split()

                if len(args) >= 3:
                    reason = ' '.join(args[2:])
                else:
                    reason = 'No reason given'

                await server.ban(user, reason=reason)
                await ctx.send(f'**{user}** has been banned')

    @commands.command()
    @commands.has_permissions(ban_members=True)
    async def unban(self, ctx, userid):
        """Unbans a member\n\n**Syntax**\n`unban member-id`\nIt should be noted that you *must* use an ID due to Discord limitations."""
        message = ctx.message
        server = message.guild
        unban_id = int(userid)
        userlist = await server.bans()
        user = None

        for entry in userlist:
            if entry.user.id == unban_id:
                user = entry.user
                break

        if user is not None:
            await server.unban(user)
            await ctx.send(f'**{user}** has been unbanned')
        else:
            await ctx.send('User not found in ban list.')

    @commands.command()
    @commands.has_permissions(manage_roles=True)
    async def mute(self, ctx):
        """Mutes a member\n\n**Syntax**\n`mute @User`"""
        message = ctx.message

        if len(message.mentions) > 0:
            for user in message.mentions:
                if message.author.guild_permissions.administrator:
                    server = user.guild
                    role = discord.utils.get(server.roles, name="silenced")
                    await user.add_roles(role)
                    await ctx.send(f'**{user}** has been muted')

    @commands.command()
    @commands.has_permissions(manage_roles=True)
    async def unmute(self, ctx):
        """Unmutes a member\n\n**Syntax**\n`unmute @User`"""
        message = ctx.message

        if len(message.mentions) > 0:
            for user in message.mentions:
                if message.author.guild_permissions.administrator:
                    server = user.guild
                    role = discord.utils.get(server.roles, name="silenced")
                    await user.remove_roles(role)
                    await ctx.send(f'**{user}** has been unmuted')

    @commands.command()
    @commands.has_permissions(manage_messages=True)
    async def purge(self, ctx):
        """Purges 'n' messages\n\n**Syntax**\n`purge number`"""
        message = ctx.message
        args = ctx.message.content.split()

        if len(args) >= 2:
            n_purge = int(args[1])
            n_purge = n_purge + 1
            channel = message.channel
            await channel.purge(limit=n_purge)

def setup(bot):
    bot.add_cog(admin(bot))