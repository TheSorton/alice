from discord.ext import commands
import discord
import random
from util import checks
from alice import channels

class logs(commands.Cog):
    """ Logging module (has no commands) """
    def __init__(self, bot, logChan, chanrules):
        self.bot = bot
        self.logChan = logChan
        self.rules_channel = chanrules

    # Welcome / Leave user messages.
    async def mem_join(self, member):
        memberID = member.id
        guild = member.guild

        msgs = [f'<@{memberID}> has joined.']

        if guild.system_channel is not None:
            await guild.system_channel.send(random.choice(msgs))

    async def mem_leave(self, member):
        guild = member.guild

        msgs = [f'**{member}** has left.']

        if guild.system_channel is not None:
            await guild.system_channel.send(random.choice(msgs))

    # Welcome / Leave user logging.
    async def log_member_join(self, member):
        logChan = self.bot.get_channel(int(self.logChan))
        name = member.name + "#" + member.discriminator
        embed = (discord.Embed(color=0x09ff00)
                        .set_author(name=name + ' has joined the server', icon_url=member.avatar_url_as(static_format='png'))
                        .set_footer(text=f'ID: {member.id}'))
        await logChan.send(embed=embed)

    async def log_member_leave(self, member):
        logChan = self.bot.get_channel(int(self.logChan))
        name = member.name + "#" + member.discriminator
        embed = (discord.Embed(color=0x1c1c1c)
                        .set_author(name=name + ' has left the server', icon_url=member.avatar_url_as(static_format='png'))
                        .set_footer(text=f'ID: {member.id}'))
        await logChan.send(embed=embed)

    # Welcome events.
    @commands.Cog.listener()
    async def on_member_join(self, member):
        await self.mem_join(member)
        await self.log_member_join(member)
        server = member.guild
        role = discord.utils.get(server.roles, name="users")
        await member.add_roles(role)

    @commands.Cog.listener()
    async def on_member_remove(self, member):
        await self.mem_leave(member)
        await self.log_member_leave(member)

    # Logging events.
    @commands.Cog.listener()
    async def on_message_delete(self, message):
        logChan = self.bot.get_channel(int(self.logChan))
        name = message.author.name + "#" + message.author.discriminator
        try:
            embed = (discord.Embed(color=0xff9d01)
                        .set_author(name=name + f" had their message deleted in #{message.channel}", icon_url=message.author.avatar_url_as(static_format='png'))
                        .add_field(name="Message", value=message.content)
                        .add_field(name="Attachment (Access Denied is likely)", value=message.attachments[0].url, inline=False)
                        .set_footer(text=f" Message ID: {message.id} • Channel ID: {message.channel.id}"))
        except IndexError:
            embed = (discord.Embed(color=0xff9d01)
                        .set_author(name=name + f" had their message deleted in #{message.channel}", icon_url=message.author.avatar_url_as(static_format='png'))
                        .add_field(name="Message", value=message.content)
                        .set_footer(text=f" Message ID: {message.id} • Channel ID: {message.channel.id}"))
 
        await logChan.send(embed=embed)


    @commands.Cog.listener()
    async def on_message_edit(self, before, after):
        logChan = self.bot.get_channel(int(self.logChan))
        if before.content == after.content:
            return

        name = before.author.name + "#" + before.author.discriminator
        embed = (discord.Embed(color=0x00ff8c)
                        .set_author(name=name + f" edited their message in #{before.channel}", icon_url=before.author.avatar_url_as(static_format='png'))
                        .add_field(name="Old Message", value=before.content, inline=False)
                        .add_field(name="New Message", value=after.content)
                        .set_footer(text=f"Message ID: {before.id}"))
        await logChan.send(embed=embed)

    # Listens only for nickname changes
    @commands.Cog.listener()
    async def on_member_update(self, before, after):
        logChan = self.bot.get_channel(int(self.logChan))
        if before.nick == after.nick:
            return

        embed = (discord.Embed(color=0x00ff8c)
                        .set_author(name=after + f" changed their nickname", icon_url=after.avatar_url_as(static_format='png'))
                        .add_field(name="Old Nickname", value=before.nick, inline=False)
                        .add_field(name="New Nickname", value=after.nick)
                        .set_footer(text=f"User ID: {after.id}"))

        await logChan.send(embed=embed)
    
    @commands.Cog.listener()
    async def on_member_update(self, before, after):
        logChan = self.bot.get_channel(int(self.logChan))
        if after.roles == before.roles:
            return

        elif len(after.roles) > len(before.roles):
            s = set(before.roles)
            newrole = [x for x in after.roles if x not in s]
            if len(newrole) == 1:
                embed = (discord.Embed(color=0x00ff8c)
                            .set_author(name=after.name + '#' + after.discriminator +  " was given a role", icon_url=after.avatar_url_as(static_format='png'))
                            .add_field(name="Given role", value=newrole[0].name)
                            .set_footer(text=f"User ID: {after.id}"))
                await logChan.send(embed=embed)

        elif len(after.roles) < len(before.roles):
            s = set(after.roles)
            newrole = [x for x in before.roles if x not in s]
            if len(newrole) == 1:
                embed = (discord.Embed(color=0xff9d01)
                        .set_author(name= after.name + '#' + after.discriminator + " had a role taken", icon_url=after.avatar_url_as(static_format='png'))
                        .add_field(name="Removed role", value=newrole[0].name)
                        .set_footer(text=f"User ID: {after.id}"))

                await logChan.send(embed=embed)


    @commands.Cog.listener()
    async def on_member_ban(self, guild, member):
        logChan = self.bot.get_channel(int(self.logChan))

        name = member.name + "#" + member.discriminator
        embed = (discord.Embed(color=0xdc143c)
                        .set_author(name=name + " was banned", icon_url=member.avatar_url_as(static_format='png'))
                        .set_footer(text=f'ID: {member.id}'))
        await logChan.send(embed=embed)

    @commands.Cog.listener()
    async def on_bulk_message_delete(self, member):
        logChan = self.bot.get_channel(int(self.logChan))
        embed = (discord.Embed(title="Messages were bulk deleted", color=0xdc143c))
        await logChan.send(embed=embed)

def setup(bot):
    if bool(checks.logCheck(channels)):
        (chanRules, chanLog) = checks.logCheck(channels)
        bot.add_cog(logs(bot, chanLog, chanRules))
