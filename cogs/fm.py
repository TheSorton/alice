
from discord.ext import commands
import discord
from util import checks, files
from alice import lfm, prefix

import csv
import pandas as pd

import requests
import json

import fnmatch, re, html2markdown
from datetime import datetime

# Add the user if fmset's check fails
def addUser(author, arg):
    with open('res/lfm.csv', 'a') as outfile:
        writer = csv.writer(outfile)
        writer.writerow([f"{author}", f"{arg}"])
        outfile.close()


# Get a username from an ID
def getUser(authorID):
    df = pd.read_csv('res/lfm.csv')
    ids = df["ID"]
    for id in ids:
        if authorID == id:
            mask = df["ID"] == authorID
            username = df.loc[mask, "Username"].values[0]
    return username


class lastfm(commands.Cog):
    """ Last.fm commands """
    def __init__(self, bot, ctx):
        self.bot = bot
        (self.api, self.secrets) = checks.lfmCheck(lfm)

    # This command writes to ../res/lfm.csv
    @commands.command()
    async def fmset(self, ctx, username):
        """ Set your last.fm username\n\n**Syntax**\n`fmset username` """
        author = ctx.message.author.id
        df = pd.read_csv('res/lfm.csv')
        ids = df["ID"]
        for id in ids:
            if author == id:
                df.loc[df["ID"] == author, "Username"] = username
                df.to_csv("res/lfm.csv", index=False)
                await ctx.send(f"Your username has been updated to **{username}**")
                break
        else:
            addUser(author, username)
            await ctx.send(f"Your username has been set to **{username}**")

    @commands.command()
    async def fm(self, ctx, username = None):
        """ Get last.fm status from either you or a mentioned user\n\n**Syntax**\n`fm @User (optional)` """
        message = ctx.message
        try:
            if username is None:
                memberID = message.author.id
                member = message.author
                username = getUser(memberID)
            elif len(message.mentions) > 0:
                memberID = message.mentions[0].id
                member = message.mentions[0]
                username = getUser(memberID)
            elif username is not None:
                username = username
                member = message.author


            url = f'http://ws.audioscrobbler.com/2.0/?method=user.getrecenttracks&user={username}&api_key={self.api}&format=json'
            userurl = f'http://ws.audioscrobbler.com/2.0/?method=user.getinfo&user={username}&api_key={self.api}&format=json'

            response = requests.get(url)
            userResponse = requests.get(userurl)

            if '@attr' in response.json()['recenttracks']['track'][0]:
                np = "Now playing"
            else:
                np = "Last played"

            embed = (discord.Embed(colour=0xfffff)
            .set_author(name=f"{np} – {username}", url=f'https://last.fm/user/{username}', icon_url=member.avatar_url_as(static_format='png'))
            .add_field(name="Artist", value=response.json()['recenttracks']['track'][0]['artist']['#text'])
            .add_field(name="Track", value=response.json()['recenttracks']['track'][0]['name'], inline=True)
            .set_thumbnail(url=response.json()['recenttracks']['track'][0]['image'][2]['#text'])
            .set_footer(text=f"{username} has {userResponse.json()['user']['playcount']} scrobbles."))
            await ctx.send(embed=embed)
        except IndexError:
            await ctx.send("Either that's not a user, or they don't have any scrobbles.")
        except KeyError:
            await ctx.send("Either that's not a user, or they don't have any scrobbles.")
    
    @commands.cooldown(rate=3, per=7, type=commands.BucketType.user)
    @commands.command()
    async def fmc(self, ctx, args='7day'):
        """ Get a last.fm collage from tapmusic.net\n\n**Syntax**\n`fmc @User timeframe (both are optional)`\n
        If no timeframe is given, it will default to 7day.\nProper timeframes are: `7day, 1month, 3month, 6month, overall`"""
        try:
            message = ctx.message
            if len(message.mentions) > 0:
                authorID = message.mentions[0].id
                member = message.mentions[0]
            else:
                authorID = message.author.id
                member = message.author
            username = getUser(authorID)
            url = f"https://www.tapmusic.net/collage.php?user={username}&type={args}&size=3x3"
            embed = (discord.Embed(color=0xfffff, title='Last.FM Collage')
                    .set_author(name=username, url=f'https://last.fm/user/{username}',
                                icon_url=member.avatar_url_as(static_format='png'))
                    .set_image(url=url)
                    .set_footer(text="Note that generation is slow and may even fail. Don't spam it"))
            await ctx.send(embed=embed)
        except UnboundLocalError:
            await ctx.send(f'Either set your username using {prefix}fmset or mention someone who has their name registered.')


    @commands.command()
    async def fmstats(self, ctx, *, username = None):
        """ Get user statistics from last.fm.\n\n**Syntax**\n`fmstats username (optional)` """
        
        username = username or ctx.message.author.id
        if username == ctx.message.author.id:
            username = getUser(username)

        user = f'http://ws.audioscrobbler.com/2.0/?method=user.getinfo&user={username}&api_key={self.api}&format=json'
        artists = f'http://ws.audioscrobbler.com/2.0/?method=user.gettopartists&user={username}&api_key={self.api}&format=json'
        user = requests.get(user)
        artists = requests.get(artists)

        topArtists = artists.json()['topartists']['artist']
        dic = {}

        for i in range(0,5,1):
            dic[i] = f"{i + 1}. {topArtists[i]['name']}  ({topArtists[i]['playcount']} plays)"
            artists = json.dumps(dic)
        str = f"{dic[0]}\n{dic[1]}\n{dic[2]}\n{dic[3]}\n{dic[4]}"

        embed = (discord.Embed(colour=0xfffff, title="Statistics")
        .set_author(name=username, url=f'https://last.fm/user/{username}', icon_url=user.json()['user']['image'][2]['#text'])
        .add_field(name="Play Count", value=user.json()['user']['playcount'])
        .add_field(name="Registered", value=datetime.utcfromtimestamp(user.json()['user']['registered']['#text']).strftime('%m/%d/%Y %H:%M') + " UTC", inline=False)
        .add_field(name="Top Artists", value=str)
        .set_thumbnail(url=user.json()['user']['image'][2]['#text']))
        await ctx.send(embed=embed)

def setup(bot):
    if bool(checks.lfmCheck(lfm)):
        files.lfmDBCheck()
        ctx = commands.Context
        bot.add_cog(lastfm(bot, ctx))
