from discord.ext import commands
import discord

import spotipy
from spotipy.oauth2 import SpotifyClientCredentials

from util import checks
from alice import spot, lfm

import re, html2markdown, fnmatch
import requests, json
import urllib

class spotify(commands.Cog):
    """ Spotify commands """
    def __init__(self, bot):
        self.bot = bot
        (self.clientID, self.clientSecret) = checks.spotifyCheck(spot)

    if bool(checks.lfmCheck(lfm)):
        @commands.command()
        async def artist(self, ctx, *, artist):
            """ Get information about an artist\n\n**Syntax**\n`artist artist-name`"""
            spotify = spotipy.Spotify(client_credentials_manager=SpotifyClientCredentials(self.clientID, self.clientSecret))

            # Search spotify for artist
            result = spotify.search(f"{artist}", type='artist')
            name = result['artists']['items'][0]['name']
            artistInfo = spotify.artist(result['artists']['items'][0]['uri'])
            
            # Get artist information from last.fm
            api = lfm['API key']
            url = f'http://ws.audioscrobbler.com/2.0/?method=artist.getinfo&artist={urllib.parse.quote(artist)}&api_key={api}&format=json'
            lfmAPI = requests.get(url)

            # Check genres
            if artistInfo['genres']:
                genre = artistInfo['genres'][0]
            else:
                genre = "Unavailable"

            # Check Biography
            matched = re.match("^ <a href=.*", lfmAPI.json()['artist']['bio']['summary'])
            if bool(matched) is not True:
                bio = lfmAPI.json()['artist']['bio']['summary']
                bio = html2markdown.convert(bio)
            else:
                bio = "Unavailable"

            # Send the embed
            embed = (discord.Embed(colour=0xfffff)
            .set_author(name=name, url=f"{artistInfo['external_urls']['spotify']}", icon_url=f"{artistInfo['images'][2]['url']}")
            .add_field(name="Genre", value=genre)
            .add_field(name="Biography", value=bio, inline=False)
            .set_thumbnail(url=f"{artistInfo['images'][0]['url']}"))
            await ctx.send(embed=embed)
    else:
        @commands.command()
        async def artist(self, ctx):
            """ Command disabled """
            await ctx.send("This command has been disabled")

def setup(bot):
    if bool(checks.spotifyCheck(spot)):
            bot.add_cog(spotify(bot))
