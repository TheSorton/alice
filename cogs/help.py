import discord
from discord.ext import commands
from alice import prefix

class docs(commands.Cog):
    """ Docmentation commands """
    def __init__(self, bot):
        self.bot = bot

    @commands.command()
    async def help(self,ctx,*cog):
        """Help command"""
        if not cog:
            halp=discord.Embed(title='Modules',
                            description=f'Use `{prefix}help *module*` to find out more about them!\nModules are case-sensitive')
            cogs_desc = ''
            for x in self.bot.cogs:
                cogs_desc += ('{} - {}'.format(x,self.bot.cogs[x].__doc__)+'\n')
            halp.add_field(name='Modules',value=cogs_desc[0:len(cogs_desc)-1],inline=False)
            await ctx.send('',embed=halp)
        else:
            """Helps me remind you if you pass too many args."""
            if len(cog) > 1:
                await ctx.send('Command error: too many modules passed.')
            else:
                """Command listing within a cog."""
                found = False
                for x in self.bot.cogs:
                    for y in cog:
                        if x == y:
                            halp=discord.Embed(title=cog[0]+' module commands',description=self.bot.cogs[cog[0]].__doc__)
                            for c in self.bot.get_cog(y).get_commands():
                                if not c.hidden:
                                    halp.add_field(name=c.name,value=c.help,inline=False)
                            found = True
                if not found:
                    """Reminds you if that cog doesn't exist."""
                    await ctx.send(f"\U0001f6ab | '**{cog[0]}**' isn't a valid module. They are case-sensitive.")

def setup(bot):
    bot.add_cog(docs(bot))