import discord
from discord.ext import commands
import platform
from datetime import datetime
import json


# Parameters. 
with open('config.json') as config:
    config = json.load(config)
    channels = config['Channels']
    googleImg = config['Google']
    roles = config["Roles"]
    prefix = config['Bot']["Prefix"]
    token = config['Bot']["Token"]
    owner = config['Bot']['Owner']
    lfm = config['Last.fm']
    spot = config['Spotify']

# Startup
bot = commands.Bot(commands.when_mentioned_or(f'{prefix}'))

# Load cogs
startup_modules = [
    "cogs.help",
    "cogs.admin",
    "cogs.owner",
    "cogs.roles",
    "cogs.utils",
    "cogs.fun",
    "cogs.image",
    "cogs.google",
    "cogs.spotify",
    "cogs.fm",
    "cogs.log",
    "util.error"
    ]

bot.remove_command('help')
for extension in startup_modules:
    try:
        bot.load_extension(extension)
    except Exception as e:
        exc = '{}: {}'.format(type(e).__name__, e)
        print('Failed to load extension {}\n{}'.format(extension, exc))

@bot.event
async def on_ready():

    print(  "Python Version: " + platform.python_version() + "\n" +
            "Discord.py Version: " + discord.__version__ + "\n" +
            "Current Time: " + datetime.now().strftime("%H:%M:%S") +
            f"\nLogged in as {bot.user}"
)



# Run it
    await bot.change_presence(status=discord.Status.online, activity=discord.Game(f"{prefix}help"))
bot.run(token, reconnect=True)

