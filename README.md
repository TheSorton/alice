# alice
alice is a bot for discord

## adding commands
alice uses [cogs](https://discordpy.readthedocs.io/en/latest/ext/commands/cogs.html) for commands.

check the cogs in `./cogs/` as an example.

## configuration
alice uses json for configuration. copy `config.example.json` to `config.json`

`config.example.json`
```jsonc
{
    "Bot": {
        "Token": "Bot Token", // Needed
        "Prefix": "Prefix", // Needed
        "Owner": "Your ID" // Needed
    },
    "Channels": {
            "Rules": "Rules Channel ID",
            "Log": "Logging Channel ID" // Needed (for error handling) 
        },
    "Google": {
        "API Key": "Google API Key", // Google Image commands will be disabled if left untouched
        "Google CX": "Google Project CX"
        },

    "Last.fm": {
            "API key": "Last.FM API Key", // Music commands will be disabled if left untouched
            "Secret": "Last.FM Shared Secret"
        },

    "Spotify": {
            "Secret": "Spotify Secret", // Music commands will be disabled if left untouched
            "ID": "Spotify Project ID"
        },    

    "Roles": [ // Role management will be disabled if left untouched. alice is set up to only accept lower-case role names. This can be removed by removing lines 17 and 33 in cogs/roles.py
        "Role 1", 
        "Role 2"
    ]
}
```


## running
first you will need to install the dependencies from `requirements.txt`. to do this, run `pip install -r requirements.txt`.

finally, run the bot with `python alice.py`. note that the bot requires python3.